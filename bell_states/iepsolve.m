function [a,b,mat] = iepsolve(spec)
%
% IEPSOLVE solves an inverse eigenvalue problem: The function constructs
% the persymmetric tridiagonal matrix of size N*N for a given set of N real
% non-degenerate eigenvalues.
%
% [A,B,MAT] = IEPSOLVE(SPEC)
% 
% SPEC is the vector containing the eigenvalues (no ordering required).
% 
% MAT is the complete persymmetric tridiagonal matrix.
% A is the vector containing the diagonal elements of MAT.
% B is the vector containing subdiagonal (superdiagonal) elements of MAT.
%
% Supplemental material to the article by M. Bruderer et al. (2011)
% 'Exploiting boundary states of imperfect spin chains for high-fidelity
%  state transfer'
%

% Determine the spectrum size N and the iteration maximum M
N = size(spec,2);
if rem(N,2)
    M = ceil(N/2);
else
    M = N/2;
end

% Shift and rescale the spectrum into the interval [-1,1]
mpos = (max(spec) + min(spec))/2;
spec = spec - mpos;
scale = max(abs(spec));
spec = spec/scale;

% Initialize the necessary arrays a_j, (b_j)^2, w_k and p_{j-1}
a = zeros(1,M);
bb = zeros(1,M);
w = zeros(1,N);
p1 = ones(1,N);

% Calculate the weights w_k from the spectrum
tmp = spec(2:N);
for  k = 1:N
    w(k) = abs(prod(1./(spec(k) - tmp)));
    tmp(k) = spec(k);
end

% Calculate the components a_1, (b_1)^2 and the polynomial p_1
nom_a = sum(spec .* w);
den = sum(w);
a(1) = nom_a / den;
p = spec - a(1);
nom_b = sum(w .* p.^2);
bb(1) = nom_b / den;

% Calculate the remaining components a_j, (b_j)^2 and the polynomials p_j
for j = 2:M
    p2 = p1;
    p1 = p;
    nom_a = sum(spec .* w .* p1.^2);
    den = sum(w .* p1.^2);
    a(j) = nom_a / den;
    p = (spec - a(j)) .* p1 - bb(j-1) .* p2;
    nom_b = sum(w .* p.^2);
    bb(j) = nom_b / den;
end

% Compile and rescale the results using symmetry properties of the matrix
a = scale * a + mpos;
b = scale * sqrt(bb);
if rem(N,2)
    a = [a(1:M-1),fliplr(a)];
    b = [b(1:M-1),fliplr(b(1:M-1))];
else
    a = [a(1:M),fliplr(a(1:M))];
    b = [b(1:M-1),fliplr(b)];
end
mat =  diag(a) + diag(b,1) + diag(b,-1);

endfunction
