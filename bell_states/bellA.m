function f = fidelity(start, final, eigenvals, t)
% Takes column vectors start, final and eigenvals of same length and returns amplitude of state start to propagate into state final after time t.
	f = (final' * (e.^(i * eigenvals * t) .* start));
endfunction

function [t fidp fidm] = calculate_fidelity(spectrum)
% Calculates fidelity of bell state transmission as a function of time for given spectrum 
	[A, B, MAT] = iepsolve(spectrum);
	[vecs, _] = eig(MAT);

	t = linspace(0, 2 * pi, 10000);
	
	% calculate eigenbasis components of initial and final states
	c_10_start = vecs \ [1 ; zeros(length(spectrum) - 1, 1)];
	c_01_end = vecs \ [zeros(length(spectrum) - 1, 1) ; 1];
	c_01_start = vecs \ [0 ; 1 ; zeros(length(spectrum) - 2, 1)];
	c_10_end = vecs \ [zeros(length(spectrum) - 2, 1); 1 ; 0];
	
	for j = 1:length(t)
	 	a(j) = 0.5 * fidelity(c_10_start, c_01_end, spectrum', t(j));
		b(j) = 0.5 * fidelity(c_01_start, c_10_end, spectrum', t(j));
		c(j) = 0.5 * (fidelity(c_10_start, c_10_end, spectrum', t(j)) + fidelity(c_01_start, c_01_end, spectrum', t(j)));
		fidp(j) = abs(a(j) + b(j) + c(j));
		fidm(j) = abs(a(j) + b(j) - c(j));
	end
endfunction


% create several spectra
N = str2num(argv(){1});
ks = -0.5 * (N - 1) : 0.5 * (N - 1);
spectrum_linear = ks .* 7;
spectrum_linear_shift = spectrum_linear - sign(spectrum_linear) * 6;
left_lin = spectrum_linear_shift(1, 1:0.5*(N-3)) + (6);
right_lin = spectrum_linear_shift(1, 0.5*(N+5):end) - (6);
spectrum_linear_sshift = [left_lin spectrum_linear_shift(1, 0.5*(N-1):0.5*(N+3)) right_lin];

spectrum_iq = ks .* (N - 1 - abs(ks));
spectrum_iq_shift = spectrum_iq - sign(spectrum_iq) * (N-3);
left = spectrum_iq_shift(1, 1:0.5*(N-3)) + (N-5);
right = spectrum_iq_shift(1, 0.5*(N+5):end) - (N-5);
spectrum_iq_sshift = [left spectrum_iq_shift(1, 0.5*(N-1):0.5*(N+3)) right];

band_gap = str2num(argv(){2})  % additional separation of centered and peripheral bands
spectrum_iq_sshift_mod = spectrum_iq_sshift;
spectrum_iq_sshift_mod(1, 1:0.5*(N-5)) += band_gap;
spectrum_iq_sshift_mod(1, 0.5*(N+7):end) -= band_gap;

% nonuplett spectrum
spectrum_iq_sshift_mod2 = spectrum_iq_sshift;
spectrum_iq_sshift_mod2(1, 1:0.5*(N-5)) += (N-7);
spectrum_iq_sshift_mod2(1, 0.5*(N+7):end) -= (N-7);
spectrum_iq_sshift_mod2(1, 1:0.5*(N-7)) += (N-9);
spectrum_iq_sshift_mod2(1, 0.5*(N+9):end) -= (N-9);


% write eigenvectors

if N == 31 && band_gap == 0
	[A B MAT] = iepsolve(spectrum_iq);
	[vecs ev] = eig(MAT);
	save eigenvecs_bellA_normal.txt vecs;
	
	[A B MAT] = iepsolve(spectrum_iq_shift);
	[vecs ev] = eig(MAT);
	save eigenvecs_bellA_shift.txt vecs;
	
	[A B MAT] = iepsolve(spectrum_iq_sshift);
	[vecs ev] = eig(MAT);
	save eigenvecs_bellA_sshift.txt vecs;
end

if N == 71 && band_gap == 60
	[A B MAT] = iepsolve(spectrum_iq_sshift_mod);
	[vecs ev] = eig(MAT);
	save eigenvecs_bellA_sshift_mod.txt vecs;
end

% write spectra and couplings 

if N == 31
	outfile_spectra = fopen('spectra.txt', 'w');
	for i = 1:N
		fprintf(outfile_spectra, '%d & %d & %d \\\n', spectrum_iq(i), spectrum_iq_shift(i), spectrum_iq_sshift(i));
	end
	fclose(outfile_spectra);

	[_ A MAT] = iepsolve(spectrum_iq);
	[_ B MAT] = iepsolve(spectrum_iq_shift);
	[_ C MAT] = iepsolve(spectrum_iq_sshift);
	disp(A);
	outfile_couplings = fopen('couplings.txt', 'w');
	for i = 1:(N-1)
		fprintf(outfile_couplings, '%.10f %.10f %.10f\n', A(i), B(i), C(i));
	end
	fclose(outfile_couplings);
end

if N == 321
	disp(spectrum_iq_sshift_mod2);
	[t fidp fidm] = calculate_fidelity(spectrum_iq_sshift_mod2);
else
	disp(spectrum_iq_sshift_mod);
	[t fidp fidm] = calculate_fidelity(spectrum_iq_sshift_mod);
end

filename = sprintf('fidelity_bellA_%d_%d.txt', N, band_gap);
outfile = fopen(filename, 'w');
for i = 1:length(t)
	fprintf(outfile, '%.15f %.15f %.15f\n', t(i), fidp(i), fidm(i));
end
fclose(outfile);
