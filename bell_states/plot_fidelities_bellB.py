from sys import argv
from matplotlib.ticker import MaxNLocator
import matplotlib.pyplot as plt
import numpy as np

# bell state (|11> + |00>)

# with zoom

for config in [(31, 20), (71, 60)]:
    N = config[0]
    band_gap = config[1]
    t, fid, absfid = np.loadtxt("fidelity_bellB_{}_{}.txt".format(N, band_gap), unpack=True)

    fig = plt.figure(figsize=(10,5), dpi=100)
    ax1 = fig.add_subplot(111)
    ax1.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
    ax1.set_xticklabels(["$0$", r"$\frac{1}{2}\pi$", r"$\vphantom{\frac{1}{2}}\pi$", r"$\frac{3}{2}\pi$", r"$2\vphantom{\frac{1}{2}}\pi$"])
    ax1.set_xlim(0, 2*np.pi)
    ax1.plot(t, absfid, 'r-')
    ax1.set_xlabel("$t$")
    ax1.set_ylabel("$|f(t)|$")
    
    ax2 = fig.add_axes((0.75, 0.55, 0.20, 0.35)) 
    ax2.set_xticks([3.1, 3.2])
    ax2.yaxis.set_major_locator(MaxNLocator(5))
    ax2.plot(t[4800: 5200], absfid[4800:5200], 'r-')
    plt.savefig("fidelity_bellB_{}_{}.pdf".format(N, band_gap))


# without zoom

for config in [(31, 0), (71, 0)]:
    N = config[0]
    band_gap = config[1]
    t, fid, absfid = np.loadtxt("fidelity_bellB_{}_{}.txt".format(N, band_gap), unpack=True)

    fig = plt.figure(figsize=(10,5), dpi=100)
    ax1 = fig.add_subplot(111)
    ax1.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
    ax1.set_xticklabels(["$0$", r"$\frac{1}{2}\pi$", r"$\vphantom{\frac{1}{2}}\pi$", r"$\frac{3}{2}\pi$", r"$2\vphantom{\frac{1}{2}}\pi$"])
    ax1.set_xlim(0, 2*np.pi)
    ax1.plot(t, absfid, 'r-')
    ax1.set_xlabel("$t$")
    ax1.set_ylabel("$|f(t)|$")
    plt.savefig("fidelity_bellB_{}_{}.pdf".format(N, band_gap))
