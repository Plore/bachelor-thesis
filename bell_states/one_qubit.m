function f = fidelity(start, final, eigenvals, t)
	f = (final' * (e.^(i * eigenvals * t) .* start));
endfunction

function [t fidp fidm] = calculate_fidelity(spectrum)
	[A, B, MAT] = iepsolve(spectrum);
	[vecs, _] = eig(MAT);

	t = linspace(0, 2 * pi, 5000);
	
	% calculate eigenbasis components of initial and final states
	c_10_start = vecs \ [1 ; zeros(length(spectrum) - 1, 1)];
	c_01_end = vecs \ [zeros(length(spectrum) - 1, 1) ; 1];
	c_01_start = vecs \ [0 ; 1 ; zeros(length(spectrum) - 2, 1)];
	c_10_end = vecs \ [zeros(length(spectrum) - 2, 1); 1 ; 0];
	
	for j = 1:length(t)
	 	a(j) = 0.5 * fidelity(c_10_start, c_01_end, spectrum', t(j));
		b(j) = 0.5 * fidelity(c_01_start, c_10_end, spectrum', t(j));
		c(j) = 0.5 * (fidelity(c_10_start, c_10_end, spectrum', t(j)) + fidelity(c_01_start, c_01_end, spectrum', t(j)));
		fidp(j) = abs(a(j) + b(j) + c(j));
		fidm(j) = abs(a(j) + b(j) - c(j));
	end
endfunction

N = 31;
ks = -0.5 * (N - 1) : 0.5 * (N - 1);
spectrum_linear = ks .* 7;
spectrum_iq = ks .* (N - 1 - abs(ks));

[A B MAT] = iepsolve(spectrum_iq);
[t fidp fidm] = calculate_fidelity(spectrum_iq);
outfile = fopen('fidelity_erratic.txt', 'w');
for j = 1:length(t)
	fprintf(outfile, '%.10f %.10f %.10f\n', t(j), fidp(j), fidm(j));
end
fclose(outfile);

