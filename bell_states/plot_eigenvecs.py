import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['font.size'] = 14

for case in ["normal", "shift", "sshift", "sshift_mod"]:
    evs = np.loadtxt("eigenvecs_bellA_{}.txt".format(case))
    plt.imshow(evs[::-1][:], interpolation='nearest', vmin=-0.75, vmax=0.75, origin='lower')
    plt.colorbar()
    plt.ylabel("Spin $j$")
    plt.xlabel("Eigenzustand $k$")
    plt.savefig("eigenvecs_bellA_{}.pdf".format(case))
    plt.clf()
