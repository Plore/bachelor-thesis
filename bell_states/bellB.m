function f = fidelity(start, final, eigenvals, t)
% Takes column vectors start, final and eigenvals of same length and returns amplitude of state start to propagate into state final after time t.
	f = (final' * (e.^(i * eigenvals * t) .* start));
endfunction

function amp = amplitude(pos_fst_in, pos_snd_in, pos_fst_out, pos_snd_out, a, b)
	amp = 0;
	if pos_fst_out == pos_fst_in
		amp += a(pos_fst_in);
	elseif pos_fst_out < pos_fst_in
		amp += b(pos_fst_out);
	elseif pos_fst_out > pos_fst_in
		amp += b(pos_fst_in);
	end

	if pos_snd_out == pos_snd_in
		amp += a(pos_snd_in);
	elseif pos_snd_out < pos_snd_in
		amp += b(pos_snd_out);
	elseif pos_snd_out > pos_snd_in
		amp += b(pos_snd_in);
	end

	if pos_snd_out != pos_snd_in && pos_fst_out != pos_fst_in
		amp = 0;
	end
	
endfunction

function i = mk_index(p_one, p_two, N)
	dist = p_two - p_one;
	l = p_one - 1;
	help = l * N - l * (l + 1) / 2;
	i = help + dist;
endfunction

N = str2num(argv(){1});

ks = -0.5 * (N - 1) : 0.5 * (N - 1);
spectrum_linear = ks .* 7;
spectrum_iq = ks .* (N - 1 - abs(ks));
spectrum_iq_shift = spectrum_iq - sign(spectrum_iq) * (N-3);
left = spectrum_iq_shift(1, 1:0.5*(N-3)) + (N-5);
right = spectrum_iq_shift(1, 0.5*(N+5):end) - (N-5);
spectrum_iq_sshift = [left spectrum_iq_shift(1, 0.5*(N-1):0.5*(N+3)) right];

band_gap = str2num(argv(){2}); % additional separation of centered and peripheral bands
spectrum_iq_sshift(1, 1:0.5*(N-5)) += band_gap;
spectrum_iq_sshift(1, 0.5*(N+7):end) -= band_gap;
disp(spectrum_iq_sshift);

[as bs MAT] = iepsolve(spectrum_iq_sshift);
%as = zeros(N, 1);
%bs = ones(N, 1);

system_length = N * (N - 1) / 2

for p_one = 1 : N-1
	for p_two = p_one + 1 : N
		for fst_off = [-1 0 1]
			for snd_off = [-1 0 1]
				p_one_orig = p_one + fst_off;
				p_two_orig = p_two + snd_off;
				row_index = mk_index(p_one, p_two, N);
				col_index = mk_index(p_one_orig, p_two_orig, N);
				if col_index > 0 && col_index < N * (N - 1) / 2 + 1 && p_one_orig < p_two_orig && p_one < p_two && p_two_orig <= N
					x = amplitude(p_one_orig, p_two_orig, p_one, p_two, as, bs);
%					printf('writing %ef at %d, %d in Hamiltonian, spin positions %d %d to %d %d\n', x, row_index, col_index, p_one_orig, p_two_orig, p_one, p_two); 
					H(row_index, col_index) = x;
				end
			end
		end
	end
end

save hamilton_alt.txt H

[vecs, ev] = eig(H);
ev = diag(ev);
%disp(ev);

c_11_start = vecs \ [1; zeros(system_length-1, 1)];
c_11_end = vecs \ [zeros(system_length - 1 , 1); 1];

t = linspace(0, 2*pi, 10000);
for j = 1:length(t)
	fid(j) = (fidelity(c_11_start, c_11_end, ev, t(j)));
	absfid(j) = abs(fid(j));
end

filename = sprintf('fidelity_bellB_%d_%d.txt', N, band_gap);
outfile = fopen(filename, 'w');
for i = 1:length(t)
	fprintf(outfile, '%.15f %.15f %.15f\n', t(i), fid(i), absfid(i));
end
fclose(outfile);
