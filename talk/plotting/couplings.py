#encoding: utf-8
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['font.size'] = 18
A, B, C = np.loadtxt("../../bell_states/couplings.txt", unpack=True)
k = np.arange(1, 31, 1)

fig = plt.figure(figsize=(10, 6), dpi=100)
ax = fig.add_subplot(111)
ax.set_ylim(-5, 220)
ax.plot(k, A, 'r-x', label="invers quadratisches Spektrum")
ax.plot(k, B, 'b-x', label="Triplett-Spektrum")
ax.plot(k, C, 'g-x', label="Quintuplett-Spektrum")
ax.set_xlim(0, 31)
ax.set_xlabel(r"Position $i$")
ax.set_ylabel(r"Kopplungen $b_i$")
ax.legend(loc="upper center")
fig.savefig("couplings.pdf")
