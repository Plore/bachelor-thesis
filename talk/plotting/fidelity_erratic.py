import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['font.size'] = 16

t, fidp, fidm = np.loadtxt("../../bell_states/fidelity_erratic.txt", unpack=True)

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
ax.plot(t, fidp, 'r-')
ax.set_xlim(0, 2*np.pi)
ax.set_xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi])
ax.set_xticklabels(["0", r"$\frac{1}{2}\pi$", r"$\pi$", r"$\frac{3}{2}\pi$", r"$2\pi$"])
ax.set_xlabel("$t$")
ax.set_ylabel(r"$|f(t)|$")
fig.savefig("fidelity_erratic.pdf")
