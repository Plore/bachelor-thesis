import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['figure.autolayout'] = False
plt.rcParams['font.size'] = 14

evs_normal = np.loadtxt("../../bell_states/eigenvecs_bellA_normal.txt")
evs_shift = np.loadtxt("../../bell_states/eigenvecs_bellA_shift.txt")
evs_sshift = np.loadtxt("../../bell_states/eigenvecs_bellA_sshift.txt")

fig = plt.figure(figsize=(8,4))

ax = fig.add_subplot(111)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
ax.set_xlabel("Eigenzustand $k$")
ax.set_ylabel("Spin $j$")

ax1 = fig.add_subplot(121)
ax1.imshow(evs_normal, interpolation='nearest', vmin=-0.75, vmax=0.75)

ax2 = fig.add_subplot(122)
im = ax2.imshow(evs_shift, interpolation='nearest', vmin=-0.75, vmax=0.75)

fig.subplots_adjust(left=0.1, top=0.87, bottom=0.13, right=0.85)

cax = plt.axes([0.88, 0.1, 0.03, 0.8])
fig.colorbar(im, cax=cax)

plt.savefig("eigenvecs_normal_shift.pdf")
